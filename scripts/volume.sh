#!/usr/bin/env bash

# You can call this script like this:
# $ ./volumeControl.sh up
# $ ./volumeControl.sh down
# $ ./volumeControl.sh mute

# Script modified from these wonderful people:
# https://github.com/dastorm/volume-notification-dunst/blob/master/volume.sh
# https://gist.github.com/sebastiencs/5d7227f388d93374cebdf72e783fbd6a

function get_active_sink {
  pactl list sinks short | grep RUNNING | sed -e 's,^\([0-9][0-9]*\)[^0-9].*,\1,'
}

function get_volume {
  pactl list sinks | sed -n -e '/RUNNING/,$p' | grep front-left | awk '{print $5}' | cut -d '%' -f 1
}

function is_mute {
  pactl list sinks | sed -n -e '/RUNNING/,$p' |  grep Mute | grep yes > /dev/null
}

function send_notification {
  iconSound="audio-volume-high"
  iconMuted="audio-volume-muted"
  if is_mute ; then
    dunstify -t 3000 -i $iconMuted -r 2593 -u normal "mute"
  else
    volume=$(get_volume)
    # Make the bar with the special character ─ (it's not dash -)
    # https://en.wikipedia.org/wiki/Box-drawing_character
    bar=$(seq --separator="─" 0 "$((volume / 5))" | sed 's/[0-9]//g')
    # Send the notification
    dunstify -t 3000 -i $iconSound -r 2593 -u normal "    $bar"
  fi
}

case $1 in
  up)
    # up the volume (+ 5%)
    pactl set-sink-volume $(get_active_sink) +5%
    send_notification
    ;;
  down)
    # down the volume (- 5%)
    pactl set-sink-volume $(get_active_sink) -5%
    send_notification
    ;;
  mute)
    # toggle mute
    pactl set-sink-mute $(get_active_sink) toggle
    send_notification
    ;;
esac
