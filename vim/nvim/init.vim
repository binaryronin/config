source ~/.vimrc

" Setting ESC to exit out of insert mode for the terminal 	
if has('nvim')
    tnoremap <Esc> <C-\><C-n>
    tnoremap <C-v><Esc> <Esc>
endif

