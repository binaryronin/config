## Vim8 and NeoVim

This setup is designed to be used by either Vim version 8 and above, or by NeoVim. 

### Installation

#### minpac

I use `minpac` to manage plugins. This needs to first be installed in order to manage the rest of the plugins used. To install `minpac` as an optional plugin do the following:
```
mkdir .config/nvim
export VIMCONFIG=/$HOME/.vim
mkdir -p $VIMCONFIG/pack/minpac/opt
cd $VIMCONFIG/pack/minpac/opt
git clone https://github.com/k-takata/minpac.git

```
Next link the appropriate files for the configurations:
```
ln -s $HOME/git/config/vim/vimrc ~/.vimrc
ln -s $HOME/git/config/vim/init.vim ~/.config/nvim/init.vim
```

Next go ahead and run vim (or nvim). Once there run `:PackUpdate` and all the plugins should be installed. They will probably scroll by pretty quick, so you can always check `:messages` to see what was installed and what wasn't. 

#### fzf

FZF is used for fuzzy file searching. While the plugin should be installed with `minpac` the actual executable is not installed and has to be still.

Installing the executable is easy, just run nvim and then then issue the command `:FZF`. It will ask you if you want to install the executable. Select yes and it will be downloaded and installed for you.

Finally make sure that your path includes the location to the executable:
```
export PATH=$PATH:/$HOME/.vim/pack/minpac/start/fzf/bin
```

#### Python Support

Neovim can support both Python 2 and Python 3 (unlike Vim where you need to choose). You can either install this via pip, or via your package manager if it's supported:

Via pacman:
```
pacman -S python2-neovim python-neovim
```

Via pip:
```
pip install --user --upgrade neovim
```

#### NeoVim Remote

I use NeoVim remote to keep myself from running nested NeoVim/Vim sessions since I use the built in terminal often. In order to use this you need to install this plugin either via `pip` or the aur:

```
pip3 install --user --upgrade neovim-remote
```

```
yay -S neovim-remote
```

The following also needs to be added to either your `.bashrc` or `.zshrc` or whatever you use:
```
# Use NeoVim remote to keep from nesting sessions
if [ -n "$NVIM_LISTEN_ADDRESS" ]; then
  if [ -x "$(command -v nvr)" ]; then
    alias nvim=nvr
  else
    alias nvim='echo "No nesting!"'
  fi
fi

```

### Code Completion

Code completion is handled via [coc.nvim](https://github.com/neoclide/coc.nvim). This only works with `nvim` currently. Make sure that `yarn` is installed before running this installation.

Once the initial installation is handled, install languages as mentioned in the documentation under "Extensions". For example, to install/use Python completion first install `python-language-server` either from pip or from your package manager. Then run `:CocInstall coc-pyls`.
