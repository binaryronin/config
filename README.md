# My Setup

Configs for my current setup. Below is a non-exhaustive list of packages.

## Packages needed

- rofi
- dunst
- qtile
- termite
- compton
- blueberry-tray
- firejail
- zsh
- USBGuard
- redshift
- synclient

## Font Packages 

- ttf-font-awesome-4
- ttf-material-wifi-icons-git
- ttf-koruri
- ttf-material-design-icons
- ttf-dejavu
