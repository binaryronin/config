# Firejail profile for DBeaver
include default.local
include globals.local

include disable-common.inc
include disable-passwdmgr.inc
include disable-programs.inc

caps.drop all
netfilter
nodbus
nonewprivs
noroot
protocol unix,inet,inet6
seccomp
shell none

whitelist ~/Downloads
whitelist ~/.eclipse
whitelist ~/.dbeaver4
noexec ~/Downloads
